from os.path import join, isdir, isfile, islink
from os import getcwd, makedirs, chdir, listdir, symlink, unlink
from json import load
from time import time
from subprocess import call


def start(file_config='config.json'):
    # Load config file
    current_dir = getcwd()
    path_file_config = join(current_dir, file_config)
    with open(path_file_config) as f:
        config = load(f)

    # Init
    path_app = join(config['path_apps'], config['app_name'])
    path_cdn = join(config['path_cdn'], config['app_name'])
    path_setting = join(config['path_setting'], config['app_name'])
    path_releases = join(path_app, 'releases')
    path_new_release = join(path_releases, '%s' % int(time()))
    path_current = join(path_app, 'current')

    if not isdir(path_app):
        makedirs(path_app)

    if not isdir(path_releases):
        makedirs(path_releases)

    if not isdir(path_cdn):
        makedirs(path_cdn)

    if not isdir(path_setting):
        makedirs(path_setting)

    makedirs(path_new_release)
    chdir(path_new_release)

    # Clone
    call(['git', 'clone', '-b', config['repository']['branch'], config['repository']['url']])

    # Move to new release
    tmp = listdir(path_new_release)
    call('mv %s/* ./' % tmp[0], shell=True)
    call('rm -fr ./%s' % tmp[0], shell=True)

    # Create symlinks to current dir
    for link in config['links_cdn']:
        path_link = join(path_cdn, link[1])
        if not isdir(path_link):
            makedirs(path_link)
        symlink(path_link, join(path_new_release, link[0]))

    # Create symlink settings files to current app
    for setting in config['link_settings']:
        path = '/'.join('%s' % s for s in setting['dir'])
        from_file = join(path_new_release, path)
        to_file = join(path_setting, setting['name'])
        if isfile(from_file) and not isfile(to_file):
            call('mv %s %s' % (from_file, to_file), shell=True)
        else:
            unlink(from_file)
        symlink(to_file, from_file)

    # Stop APP
    call(config['commands']['stop'], shell=True)

    # Remove old current link if exist
    if islink(path_current):
        unlink(path_current)

    # Create symlink to current
    symlink(path_new_release, path_current)

    # Start APP
    call(config['commands']['start'], shell=True)

    chdir(current_dir)