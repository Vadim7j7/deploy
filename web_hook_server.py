from BaseHTTPServer import BaseHTTPRequestHandler, HTTPServer
import threading

import run_deploy


def run_task():
    run_deploy.start()

class MyHandler(BaseHTTPRequestHandler):
    # do_POST
    def do_GET(self):
        try:
            if self.path == '/deploy/promomo?secret=ssssss':
                t = threading.Timer(1, run_task)
                t.start()

            self.send_response(200)
            self.send_header('Content-type', 'text/html')
            self.end_headers()
            self.wfile.write('Ok')

            return
        except IOError as er:
            print(er)
            self.send_error(404,'File Not Found: %s' % self.path)

def main():
    try:
        server = HTTPServer(('', 6001), MyHandler)
        server.serve_forever()
    except KeyboardInterrupt:
        server.socket.close()

if __name__ == '__main__':
    main()
